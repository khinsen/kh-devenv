;;;; System definition
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(asdf:defsystem #:kh-devenv
  :description "My personal Common Lisp development environment"
  :author "Konrad Hinsen <konrad.hinsen@fastmail.net>"
  :license  "GPL"
  :version "0.0.1"
  :serial t
  :depends-on (#:quickproject
               #:trivial-open-browser
               #:clog-moldable-inspector
               #:html-inspector-views
               #:html-inspector-views/standard)
  :components ((:file "package")
               (:file "kh-devenv")
               (:file "transcript")))
