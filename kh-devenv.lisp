;;;; Configuration of dependencies
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package :kh-devenv)

;;
;; Configure quickproject
;;
(setf quickproject:*author* "Konrad Hinsen <konrad.hinsen@fastmail.net>")
(setf quickproject:*license* "GPL")
(setf quickproject:*include-copyright* t)

;;
;; Shorthand for clog inspector
;;

(defun inspect (&optional (object html-inspector-views/standard:*image*))
  (clog-moldable-inspector:clog-inspect :object object))

(defun inspect-image ()
  (inspect))

(setf (fdefinition 'inspect-package)
         #'clog-moldable-inspector:clog-inspect-package)

(setf (fdefinition 'inspect-system)
         #'clog-moldable-inspector:clog-inspect-system)

(setf (fdefinition 'inspect-class)
         #'clog-moldable-inspector:clog-inspect-class)

#+quicklisp
(defun inspect-quicklisp ()
  (inspect html-inspector-views:*quicklisp*))

(defun inspector-tutorial ()
  (clog-moldable-inspector:tutorial))
