;;;; Package definition
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(defpackage :kh-devenv
  (:nicknames :kh)
  (:use :cl)
  (:local-nicknames (:hv :html-inspector-views)
                    (:hvs :html-inspector-views/standard))
  (:shadow #:inspect)
  (:export #:inspect
           #:inspect-image
           #:inspect-package
           #:inspect-system
           #:inspect-class
           #:inspect-quicklisp
           #:inspector-tutorial
           #:to-transcript
           #:inspect-transcript))
