;;;; Basic transcript for objects
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package :kh-devenv)

(defclass transcript ()
  ((items :initform (fset:empty-seq))))

(defvar *transcript* (make-instance 'transcript))

(hv:defview hv:👀items (transcript transcript)
  (hv:enumerated-list-view
   (hv:thunk (fset:convert 'vector (slot-value transcript 'items)))
   :title "Items" :priority 1))

(defmethod hv:title-bar-action-buttons
    ((transcript transcript))
  (hv:action-button
   "Clear"
   (hv:thunk (setf (slot-value transcript 'items) (fset:empty-seq))
             t)))

(defun to-transcript (object)
  (fset:push-last (slot-value *transcript* 'items) object)
  object)

(defun inspect-transcript ()
  (inspect *transcript*))
